# Tutorial
###  MySql
*  [How to Deploy MySQL Server 5.7 to Kubernetes](https://www.serverlab.ca/tutorials/containers/kubernetes/how-to-deploy-mysql-server-5-7-to-kubernetes/)
*  [Reset the MySQL Root Password](http://www.ihp.sinica.edu.tw/dashboard/docs/reset-mysql-password.html) 
###  MongoDB
*  [MGOB — A MongoDB backup agent for Kubernetes](https://medium.com/google-cloud/mgob-a-mongodb-backup-agent-for-kubernetes-cfc9b30c6c92)

### RBAC
*  [RBAC with Kubernetes in Minikube](https://medium.com/@HoussemDellai/rbac-with-kubernetes-in-minikube-4deed658ea7b)

###  Headless Service
*  [k8s-Service-Overview](https://godleon.github.io/blog/Kubernetes/k8s-Service-Overview/)

### Paser Json
*  [How to parser json in golang](https://ithelp.ithome.com.tw/articles/10205062?sc=iThelpR)
